use async_openai as openai;
use openai::{types::CreateCompletionRequest, Client, Completion};
use std::env;
use teloxide::{prelude::*, utils::command::BotCommands};

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    log::info!("Starting throw dice bot...");

    let bot = Bot::from_env();

    Command::repl(bot, answer).await;
}

async fn get_completion_from_text(text: String) -> Option<String> {
    let client = Client::new().with_api_key(
        env::var("OPENAI_TOKEN").expect("'OPENAI_TOKEN' must be set in environment variable."),
    );
    let request = CreateCompletionRequest {
        model: "text-davinci-003".to_string(),
        prompt: Some(text),
        max_tokens: Some(1000u16),
        ..Default::default()
    };
    let response = Completion::create(&client, request).await.unwrap();
    response.choices.first().map(|choice| choice.text.clone())
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase", description = "User commands avaible")]
enum Command {
    #[command(description = "display this text")]
    Help,
    #[command(description = "Chat with the davinci-003 model of OpenAI")]
    TextDavinci003 { text: String },
    #[command(description = "Dice")]
    Dice,
}

async fn answer(bot: Bot, msg: Message, cmd: Command) -> ResponseResult<()> {
    match cmd {
        Command::Help => help(&bot, &msg).await?,
        Command::Dice => dice(&bot, &msg).await?,
        Command::TextDavinci003 { text } => text_davinci003(&bot, &msg, text).await?,
    }
    Ok(())
}

async fn help(bot: &Bot, msg: &Message) -> ResponseResult<()> {
    bot.send_message(msg.chat.id, Command::descriptions().to_string())
        .await?;
    Ok(())
}

async fn dice(bot: &Bot, msg: &Message) -> ResponseResult<()> {
    bot.send_dice(msg.chat.id).await?;
    Ok(())
}

async fn text_davinci003(bot: &Bot, msg: &Message, text: String) -> ResponseResult<()> {
    if let Some(text) = get_completion_from_text(text).await {
        bot.send_message(msg.chat.id, text).await?;
    }
    Ok(())
}
